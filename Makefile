IMAGE_NAME=i_pararius_watcher
CONTAINER_NAME=c_pararius_watcher

.PHONY: build
build:
	docker build -t $(IMAGE_NAME) .

.PHONY: run
run:
	docker run -d --name $(CONTAINER_NAME) -v `pwd`:/app --restart unless-stopped $(IMAGE_NAME)

.PHONY: runit
runit:
	docker run --rm -it --name $(CONTAINER_NAME) -v `pwd`:/app $(IMAGE_NAME)

.PHONY: stop
stop:
	docker stop $(CONTAINER_NAME)

.PHONY: start
start:
	docker start $(CONTAINER_NAME)

.PHONY: rm
rm:
	docker rm $(CONTAINER_NAME)

.PHONY: attach
attach:
	docker exec -it $(CONTAINER_NAME) bash

.PHONY: logs
logs:
	docker logs -f $(CONTAINER_NAME)