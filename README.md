# STEPS
### 1. Prepare config.yml file
* format:
```
telegram-bot-token: "5_________:___________________OA"
notify-chat-ids:
  - "2______7"
watch-cities:
  - "amsterdam"
  - "haarlem"
```
* get `telegram-bot-token` from BotFather
* get your chat_id from https://t.me/username_to_id_bot
* dont forget to `/start` bot before running this script

### 2. Run in docker
There is a Makefile to make it easier:
* `make build` - build an image
* `make run` - run container
* `make stop` - stop the container
* `make start` - start the container
* `make rm` - remove container
* `make logs` - watch container logs
