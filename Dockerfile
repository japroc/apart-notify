FROM python:3.8-slim-buster

WORKDIR /app

RUN pip3 install --upgrade pip && pip3 install requests pyyaml

CMD ["python3", "main.py"]
