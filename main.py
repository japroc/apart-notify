import requests
import json
import re
import shelve
import time
import yaml
import os

def get_option(name, default=None):
    with open("config.yml", "r") as ymlfile:
        cfg = yaml.safe_load(ymlfile)
    return cfg[name] if name in cfg else default

NOTIFY_CHAT_IDS = get_option("notify-chat-ids", default=[])
TELEGRAM_BOT_TOKEN = get_option("telegram-bot-token")
WATCH_CITIES = get_option("watch-cities", default=["amsterdam", "haarlem"])
STATE_FILE = get_option("state-file", "pararius_watcher_state.db")

class Apartment:
    def __init__(self, path, title, location, price, features):
        self.path = path
        self.title = title
        self.location = location
        self.price = price
        self.features = features
        self.city = None

    def get_city(self):
        return str(self.city)

    def get_db_key(self):
        return "{}-{}".format(self.city, self.path)

    def get_db_prefix(self):
        return "{}".format(self.city)
    
    def get_area_full(self):
        return self.features.get("surface-area")
    
    def is_upholstered(self):
        return self.get_interior() == 'Upholstered'

    def get_area(self):
        area = self.features.get("surface-area", '')
        m = re.search(r'\d+', area)
        return int(m.group()) if m else None

    def get_rooms_full(self):
        return self.features.get("number-of-rooms")

    def get_rooms(self):
        rooms = self.features.get("number-of-rooms", '')
        m = re.search(r'\d+', rooms)
        return int(m.group()) if m else None

    def get_price(self):
        m = re.search(r'([€,\d]+) per month', self.price)
        if not m:
            return self.price
        return m.groups()[0] if m else self.price

    def get_price_per_month(self):
        m = re.search(r'([€,\d]+) per month', self.price)
        if not m:
            return None
        value = re.sub("[^\d]", "", m.groups()[0])
        return int(value) if value.isdigit() else None

    def get_interior(self):
        return self.features.get("interior")

    def get_url(self):
        return "https://www.pararius.com" + self.path
        
    def __repr__(self):
        return "<{}(title='{}', price='{}', area='{}', rooms='{}')>".format(
            self.__class__.__name__, self.title, self.get_price(), self.get_area_full(), self.get_rooms_full())


def parse_section(section):
    title_match = re.search(r'<h2 class="listing-search-item__title">.*?</a>', section, re.S)
    if not title_match:
        return None
    title_html = title_match.group()
    m = re.search(r'<a href="([^"]*)".*?">(.*?)</a>', title_html, re.S)
    if not m:
        return None
    path, title = m.groups()
    title = title.strip()

    ## LOCATION START

    m = re.search(r'<div class="listing-search-item__location">(.*?)</div>', section, re.S)
    if not m:
        m = re.search(r'<div class="listing-search-item__sub-title">(.*?)</div>', section, re.S)
        if not m:
            return None
    location = m.groups()[0].strip()

    ## LOCATION END

    m = re.search(r'<div class="listing-search-item__price">(.*?)</div>', section, re.S)
    if not m:
        return None
    price = m.groups()[0].strip()

    features = dict()
    features_html = re.search(r'<ul class="illustrated-features.*?</ul>', section, re.S)
    if not features_html:
        return None
    m = re.findall(r'<li class="illustrated-features__item illustrated-features__item--(.*?)">(.*?)</li>', features_html.group(), re.S)
    for k, v in m:
        features[k] = v
    
    return Apartment(path, title, location, price, features)

def fetch_pararius_page(url, city=None):

    r = requests.get(url)

    apartments = list()
    sections = re.findall(r'<section class="listing-search-item listing-search-item--list listing-search-item--for-rent">(.*?)</section>', r.text, re.S)
    for section in sections:
        apart = parse_section(section)
        apart.city = city
        apartments.append(apart)
    return apartments

def fetch_pararius(city="amsterdam"):
    # base_url = "https://www.pararius.com/apartments/amsterdam/apartment/1000-2250/1-bedrooms/upholstered/page-"
    base_url = "https://www.pararius.com/apartments/{}/1000-2500/1-bedrooms/upholstered/page-".format(city)

    all_apartments = list()
    for i in range(1, 100):
        url = base_url + str(i)
        apartments = fetch_pararius_page(url, city=city)
        all_apartments.extend(apartments)
        if len(apartments) < 30:
            break
    return all_apartments

def filter_aparts(apartments, min_price=1000, max_price=2500, min_rooms=2, max_rooms=2, min_area=0, only_upholstered=True):
    apartments = filter(lambda x: not x.get_price_per_month() or x.get_price_per_month() >= min_price, apartments)
    apartments = filter(lambda x: not x.get_price_per_month() or x.get_price_per_month() <= max_price, apartments)
    apartments = filter(lambda x: not x.get_rooms() or x.get_rooms() >= min_rooms, apartments)
    apartments = filter(lambda x: not x.get_rooms() or x.get_rooms() <= max_rooms, apartments)
    apartments = filter(lambda x: not x.get_area() or x.get_area() >= min_area, apartments)
    if only_upholstered:
        apartments = filter(lambda x:x.is_upholstered(), apartments)
    return list(apartments)

def send_telegram_message(message: str, chat_id: str, api_key: str):
    headers = {'Content-Type': 'application/json'}
    data_dict = {'chat_id': chat_id, 'text': message, 'parse_mode': 'Markdown'}
    data = json.dumps(data_dict)
    url = f'https://api.telegram.org/bot{api_key}/sendMessage'
    response = requests.post(url, data=data, headers=headers)
    return response

def format_apart(apart):
    msg = apart.title + " (in {})".format(apart.get_city().capitalize()) + "\n"
    msg += apart.get_url() + "\n"
    msg += "price: {}".format(apart.price) + "\n"
    msg += "rooms: {}".format(apart.get_rooms_full()) + "\n"
    msg += "area: {}".format(apart.get_area_full())
    return msg

def new_apart_notify(apart):
    message = format_apart(apart)
    for chat_id in NOTIFY_CHAT_IDS:
        res = send_telegram_message(message, chat_id, TELEGRAM_BOT_TOKEN)
        if res.status_code != 200:
            print(res.json())
            return False
    return True

def clear_old_entries_from_db(cur_db_keys, prefix=""):
    db = shelve.open(STATE_FILE)
    for k in db.keys():
        if k.startswith(prefix) and k not in cur_db_keys:
            del db[k]
    db.close()

def find_new_entries_and_notify(filtered_apartments):
    new_entries = 0
    for apart in filtered_apartments:
        db = shelve.open(STATE_FILE)
        known = apart.path in db
        db.close()
        if known:
            continue
        
        ok = new_apart_notify(apart)
        if not ok:
            continue
        
        new_entries += 1

        db = shelve.open(STATE_FILE)
        db[apart.path] = apart
        db.close()
    
    return new_entries

def scan_city(city):
    apartments = fetch_pararius(city=city)
    filtered_apartments = filter_aparts(apartments)
    cur_db_keys = set(map(lambda x:x.get_db_key(), filtered_apartments))
    clear_old_entries_from_db(cur_db_keys, prefix=city)
    return find_new_entries_and_notify(filtered_apartments)

def loop():
    prev_time = time.time() - 60*60

    while True:
        if time.time() - prev_time < 60*60:
            time.sleep(5)
            continue

        new_entries = 0
        for city in WATCH_CITIES:
            new_entries += scan_city(city)

        if not new_entries:
            for chat_id in NOTIFY_CHAT_IDS:
                send_telegram_message("no new entries found", chat_id, TELEGRAM_BOT_TOKEN)

        prev_time = time.time()


if __name__ == "__main__":
    loop()
